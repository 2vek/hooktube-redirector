+function() {

    const defaultOptions = {
        hk_search: false,
        hk_channel: false,
        disable_polymer: false,
        channelredirect: false,
        addon_enabled: true
    };

    const hkSearchFlag = document.querySelector('#hk_search');
    const hkChannelFlag = document.querySelector('#hk_channel');
    const disablePolymerFlag = document.querySelector('#disable_polymer');
    const channelRedirectFlag = document.querySelector('#channelredirect');
    const addonEnabledFlag = document.querySelector('#addon_enabled');

    async function restoreOptions() {
        const savedOptions = await browser.storage.local.get(defaultOptions);
        hkSearchFlag.checked = savedOptions.hk_search;
        hkChannelFlag.checked = savedOptions.hk_channel;
        disablePolymerFlag.checked = savedOptions.disable_polymer;
        channelRedirectFlag.checked = savedOptions.channelredirect;
        addonEnabledFlag.checked = savedOptions.addon_enabled;

        const platform = await browser.runtime.getPlatformInfo();
        if (platform.os === 'android') {
            document.querySelectorAll('.hide-on-android').forEach(function(item) {
                item.style.display = 'none';
            });
        }
    }

    async function saveOptions() {
        const currentOptions = {
            hk_search: hkSearchFlag.checked,
            hk_channel: hkChannelFlag.checked,
            disable_polymer: disablePolymerFlag.checked,
            channelredirect: channelRedirectFlag.checked,
            addon_enabled: addonEnabledFlag.checked
        };
        await browser.storage.local.set(currentOptions);
    }

    document.addEventListener('DOMContentLoaded', restoreOptions);

    hkSearchFlag.addEventListener('change', saveOptions);
    hkChannelFlag.addEventListener('change', saveOptions);
    disablePolymerFlag.addEventListener('change', saveOptions);
    channelRedirectFlag.addEventListener('change', saveOptions);

}();