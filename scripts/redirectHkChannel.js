+function() {

    async function handleRedirect(request) {
        const platform = await browser.runtime.getPlatformInfo();
        let url = new URL(request.url);
        url.hostname = (platform.os === 'android') ? 'm.youtube.com' : 'www.youtube.com';
        const newUrl = url.toString();
        return {
            redirectUrl: newUrl
        };
    }

    browser.storage.onChanged.addListener(function (changes, area) {
        if (area == 'local' && 'hk_channel' in changes) {
            updateListener();
        }
    });

    async function updateListener() {
        const defaultOption = {
            hk_channel: false
        };
        const savedOptions = await browser.storage.local.get(defaultOption);
        if (savedOptions.hk_channel) {
            browser.webRequest.onBeforeRequest.addListener(
                handleRedirect,
                {
                    urls: [
                        '*://hooktube.com/channel/*',
                    ],
                    types: ['main_frame']
                },
                ['blocking']
            );
        } else {
            browser.webRequest.onBeforeRequest.removeListener(handleRedirect);
        }
    }

    updateListener();

}();