+function() {

    async function handleRedirect(request) {
        const platform = await browser.runtime.getPlatformInfo();
        let url = new URL(request.url);
        url.hostname = (platform.os === 'android') ? 'm.youtube.com' : 'www.youtube.com';
        const newUrl = url.toString();
        return {
            redirectUrl: newUrl
        };
    }

    browser.storage.onChanged.addListener(function (changes, area) {
        if (area == 'local' && 'hk_search' in changes) {
            updateListener();
        }
    });

    async function updateListener() {
        const defaultOption = {
            hk_search: false
        };
        const savedOptions = await browser.storage.local.get(defaultOption);
        if (savedOptions.hk_search) {
            browser.webRequest.onBeforeRequest.addListener(
                handleRedirect,
                {
                    urls: [
                        '*://hooktube.com/results*',
                    ],
                    types: ['main_frame']
                },
                ['blocking']
            );
        } else {
            browser.webRequest.onBeforeRequest.removeListener(handleRedirect);
        }
    }

    updateListener();

}();