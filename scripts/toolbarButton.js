+function() {

    const defaultOption = {
        addon_enabled: true
    };

    async function updateButton(os, Option) {
        const platform = os || await browser.runtime.getPlatformInfo();
        if (platform.os !== 'android') {
            const savedOptions = Option || await browser.storage.local.get(defaultOption);
            if (savedOptions.addon_enabled) {
                await browser.browserAction.setTitle({title: 'hooktube Redirector'});
                await browser.browserAction.setIcon({path: '../assets/hooktube-redirector-icon-48.png'});
            } else {
                await browser.browserAction.setTitle({title: 'hooktube Redirector (off)'});
                await browser.browserAction.setIcon({path: '../assets/hooktube-redirector-icon-48-mono.png'});
            }
        }
    }

    async function toggleButton() {
        const platform = await browser.runtime.getPlatformInfo();
        if (platform.os === 'android') {
            await browser.runtime.openOptionsPage();
        } else {
            const savedOptions = await browser.storage.local.get(defaultOption);
            const updatedOptions = {addon_enabled: !savedOptions.addon_enabled};
            await browser.storage.local.set(updatedOptions);
            await updateButton('desktop', updatedOptions);
        }
    }

    browser.browserAction.onClicked.addListener(toggleButton);

    browser.runtime.onInstalled.addListener(updateButton);
    browser.runtime.onStartup.addListener(updateButton);

}();