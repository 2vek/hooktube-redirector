## hooktube Redirector

This addon is **DEPRECATED** in favor of [Alternate Tube Redirector](https://gitlab.com/2vek/alternate-tube-redirector)

![hooktube Redirector icon](assets/hooktube-redirector-icon-96.png)

Redirects YouTube video links to hooktube.

more info at [mozilla addon page](https://addons.mozilla.org/en-US/firefox/addon/hooktube-redirector/).
